package com.eversoft.exception;

public class NotFoundException extends BaseException {
    public NotFoundException() {
        super(ErrorCode.NOT_FOUND);
    }
}
