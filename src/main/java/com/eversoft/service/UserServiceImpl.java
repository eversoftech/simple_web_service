package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.User;
import com.eversoft.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public UserDto getUserByEmail(String email) {
        return modelMapper.map(
                userRepository.findByEmail(email).orElseThrow(NotFoundException::new), UserDto.class
        );
    }

    @Override
    public UserDto saveUser(UserDto userDto) {
        return modelMapper.map(
                userRepository.save(modelMapper.map(userDto, User.class)), UserDto.class
        );
    }
}
