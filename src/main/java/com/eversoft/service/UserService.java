package com.eversoft.service;

import com.eversoft.dto.UserDto;

public interface UserService {
    UserDto getUserByEmail(String email);

    UserDto saveUser(UserDto userDto);
}
