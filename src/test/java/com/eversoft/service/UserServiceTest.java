package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.model.User;
import com.eversoft.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    private UserService userService;

    @Before
    public void setUp() {
        ModelMapper modelMapper = new ModelMapper();
        userService = new UserServiceImpl(userRepository, modelMapper);
    }

    @Test
    public void testFindUserByEmail() {
        final Long id = 1L;
        final String email = "sylwia";

        when(userRepository.findByEmail(email))
                .thenReturn(Optional.of(User.builder()
                        .id(1L)
                        .email(email)
                        .build()));

        UserDto userDto = userService.getUserByEmail(email);

        Assert.assertEquals(email, userDto.getEmail());
        Assert.assertEquals(id, userDto.getId());
    }
}
